/*
export function someAction (context) {
}
*/

export function fetchCarouselItems ({ commit }) {
  commit('setCarouselItems',
    {
      payload: [
        {
          id: 1,
          days: '14',
          price: '2500',
          src: 'carousel-1.jpg',
          title: 'Lorem ipsum dolor sit amet',
          content: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.'
        },
        {
          id: 2,
          days: '14',
          price: '2500',
          src: 'carousel-2.jpg',
          title: 'Lorem ipsum dolor sit amet',
          content: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.'
        },
        {
          id: 3,
          days: '14',
          price: '2500',
          src: 'carousel-3.jpg',
          title: 'Lorem ipsum dolor sit amet',
          content: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.'
        }
      ],
      loading: false,
      error: false,
      meta: null
    })
}
