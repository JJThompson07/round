/*
export function someAction (context) {
}
*/

export function fetchAllPages ({ commit }) {
  commit('setPages',
    {
      payload: [
        {
          key: 'about',
          name: 'About us',
          path: '/about'
        },
        {
          key: 'team',
          name: 'Team',
          path: '/team'
        },
        {
          key: 'services',
          name: 'Services',
          path: '/services'
        },
        {
          key: 'contact',
          name: 'Contact us',
          path: '/contact'
        }
      ],
      loading: false,
      error: false,
      meta: null
    })
}
