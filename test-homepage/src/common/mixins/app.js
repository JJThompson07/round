import { mapState, mapActions } from 'vuex'

export default {
  data () {
    return {
      pageList: [],
      carouselItems: []
    }
  },
  created () {
    this.fetchAllPages()
    this.fetchCarouselItems()
  },
  computed: {
    ...mapState('pages', [
      'allPages'
    ]),
    ...mapState('carousel', [
      'allCarouselItems'
    ])
  },
  methods: {
    ...mapActions('pages', [
      'fetchAllPages'
    ]),
    ...mapActions('carousel', [
      'fetchCarouselItems'
    ])
  },
  watch: {
    allPages () {
      this.pageList = this.allPages.payload
    },
    allCarouselItems () {
      this.carouselItems = this.allCarouselItems.payload
    }
  }
}
